<?php

/**
 * @file
 * Vivus module provide awesome and cool svg animate.
 */

use Drupal\Core\Installer\InstallerKernel;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Implements hook_help().
 */
function vivus_ui_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.vivus_ui':
      $output  = '<h3 class="vivus__about">' . t('About') . '</h3>';
      $output .= '<div class="vivus__welcome bloc bloc-head" id="vivusUI"></div>';
      $output .= '<p>' . t('<b>Vivus</b>, bringing your SVGs to life') . '</p>';
      $output .= '<p>' . t('Vivus UI is a innovative and powerful Drupal module that provides svg animate, enhancing site interactivity with awesome animations.') . '</p>';
      $output .= '<h3 class="vivus__usage">' . t('Usage') . '</h3>';
      $output .= '<dl>';
      $output .= '<dt class="vivus__title"><strong>' . t('Vivus admin page') . '</strong></dt>';
      $output .= '<dd><p>' . t('Go to <a href=":vivus_admin">Svg animate</a> admin overview page in your Drupal administration structure menu.', [':vivus_admin' => Url::fromRoute('vivus.admin')->setAbsolute()->toString()]) . '</p></dd>';
      $output .= '<dd><p>' . t('Click on <a href=":add_animation">Add animation</a> button in top of the vivus admin overview page.', [':add_animation' => Url::fromRoute('vivus.add')->setAbsolute()->toString()]) . '</p>';
      $output .= '<dd><p>' . t('Enter valid existing svg ID without hash(#) prefix. Make sure your svg already has a unique id.') . '</p>';
      $output .= '<pre><code>';
      $output .= 'Example: "mySvgID"' . "\n";
      $output .= '</code></pre></dd>';
      $output .= '<dd class="vivus__line"><p>' . t('Then you can add one or more animation by select animation type <br>and click on "Add more" for more animations.') . '</p></dd>';
      $output .= '<dd class="vivus__line"><p>' . t('You can change some setting options if you need such as:') . '</p></dd>';
      $output .= '<div class="vivus__list"><ul>';
      $output .= '<li><strong>' . t('Start') . '</strong><br><em>' . t('Defines how to trigger the animation (In viewport once the SVG is in the viewport, Manual gives you the freedom to call draw method to start, Auto start makes it start right now).') . '</em></li>';
      $output .= '<li><strong>' . t('Switch animation') . '</strong><br><em>' . t('Defines how animations change when a loop is enabled or has more than one animation (Default once the SVG will replace by another animation draw, Erase gives you rewind draw animation, Fade out makes disappear to start next animation).') . '</em></li>';
      $output .= '<li><strong>' . t('Loop') . '</strong><br><em>' . t('If enabled, svg will continue the animation.') . '</em></li>';
      $output .= '<li><strong>' . t('Initial delay') . '</strong><br><em>' . t('Delay before starting your animation.') . '</em></li>';
      $output .= '<li><strong>' . t('Min display time') . '</strong><br><em>' . t('The minimum display time for each animation before replacing.') . '</em></li>';
      $output .= '<li><strong>' . t('Duration') . '</strong><br><em>' . t('Animation duration, in frames.') . '</em></li>';
      $output .= '<li><strong>' . t('Delay') . '</strong><br><em>' . t('Time between the drawing of first and last path, in frames (only for delayed animations).') . '</em></li>';
      $output .= '<li><strong>' . t('Path timing') . '</strong><br><em>' . t('Timing animation function for each path element of the SVG.') . '</em></li>';
      $output .= '<li><strong>' . t('Reverse stack') . '</strong><br><em>' . t('Reverse the order of execution. The default behaviour is to render from the first "path" in the SVG to the last one. This option allow you to reverse the order.') . '</em></li>';
      $output .= '<li><strong>' . t('Dash gap') . '</strong><br><em>' . t('Whitespace extra margin between dashes. Increase it in case of glitches at the initial state of the animation.') . '</em></li>';
      $output .= '<li><strong>' . t('Force render') . '</strong><br><em>' . t('Force the browser to re-render all updated path items. By default, the value is true on IE only.') . '</em></li>';
      $output .= '<li><strong>' . t('Self destroy') . '</strong><br><em>' . t('Removes all extra styling on the SVG, and leaves it as original.') . '</em></li>';
      $output .= '</ul></div>';
      $output .= '<dd class="vivus__text"><p>' . t('Save animations and that`s it, Enjoy svg animation with Vivus!') . '</p></dd>';
      $output .= '<br>';
      $output .= '<dt class="vivus__title"><strong>' . t('Vivus global configuration page') . '</strong></dt>';
      $output .= '<dd class="vivus__line"><p>' . t('Go to <a href=":vivus_settings">Vivus</a> config page in your Drupal configuration menu.', [':vivus_settings' => Url::fromRoute('vivus.settings')->setAbsolute()->toString()]) . '</p></dd>';
      $output .= '<dd class="vivus__line"><p>' . t('You can change default setting options if you need such as:') . '</p></dd>';
      $output .= '<div class="vivus__list"><ul>';
      $output .= '<li><strong>' . t('Load Vivus') . '</strong><br><em>' . t('If enabled, this module will attempt to load the vivus plugin for your site.') . '</em></li>';
      $output .= '<li><strong>' . t('Load on Specific URLs') . '</strong><br><em>' . t('If you use the vivus only for a special section or pages, you can control to load the vivus plugin to that page to improve performance.') . '</em></li>';
      $output .= '<li><strong>' . t('Default Options') . '</strong><br><em>' . t('You can change the default settings of Vivus options so that you don`t need to customize every time in the "Add animation" form.') . '</em></li>';
      $output .= '<li><strong>' . t('Additional Options') . '</strong><br><em>' . t('Set default settings for extended vivus options so that you don`t need to customize every time in the "Add animation" form.') . '</em></li>';
      $output .= '<li><strong>' . t('Animation Preview') . '</strong><br><em>' . t('You can test your desired animation with its settings and see the result.') . '</em></li>';
      $output .= '</ul></div>';
      $output .= '</dl>';
      $output .= '<br>';
      $output .= '<h3 class="vivus__title">' . t('Additional Information') . '</h3>';
      $output .= '<p class="vivus__text">' . t('Pick existing svg ID you want to animate to it by using right-click on your website, choose "Inspect" or "View page source (CTRL + U)" of context menu in your Chrome browser and find exact valid svg ID.') . '</p>';
      $output .= '<div class="vivus__list"><ul>';
      $output .= '<li><strong>' . t('Use svg ID without hash(#) prefix.');
      $output .= '</strong><br>e.g. Find id in your svg tag: &lt;svg id="mySvgUniqueID"...&gt; then your svg exact ID is "mySvgUniqueID" etc.</li>';
      $output .= '</ul></div>';
      $output .= '<p class="vivus__warning"><strong>' . t('IMPORTANT: If your svg does not have an ID, you need to add a specific ID to your svg tag (for svg file edit in some editor such as "Notepad" or "Notepad++").') . '</strong></p>';
      $output .= '<p>' . t('You can add inline svg in your theme or using Drupal contribute modules such as <a href=":svg_image">Svg Image</a> or <a href=":svg_image_field">SVG Image Field</a>.',
          [
            ':svg_image' => 'https://www.drupal.org/project/svg_image',
            ':svg_image_field' => 'https://www.drupal.org/project/svg_image_field',
          ]) . '</p>';
      return $output;
  }
}

/**
 * Implements hook_page_attachments().
 */
function vivus_ui_page_attachments(array &$attachments) {
  // Don't add the library during installation.
  if (InstallerKernel::installationAttempted()) {
    return;
  }

  // Load the Vivus configuration settings.
  $config = \Drupal::config('vivus.settings');

  // Checking the path to load vivus for demo examples on help page.
  $currentPath = Url::fromRoute('<current>')->toString();
  $helpPath = Url::fromRoute('help.page', ['name' => 'vivus_ui'])->toString();

  if (\Drupal::service('path.matcher')->matchPath($currentPath, $helpPath)) {
    $module_path = '/' . \Drupal::service('extension.list.module')->getPath('vivus_ui');
    $sample_path = "$module_path/img/hi-there.svg";

    // Export welcome svg animation.
    $attachments['#attached']['drupalSettings']['vivus']['welcome'] = $sample_path;

    // Attach and init vivus.
    $attachments['#attached']['library'][] = 'vivus_ui/vivus.settings';
    return TRUE;
  }

  // Don't include Vivus library if the user has opted out of loading it.
  if (!$config->get('load')) {
    return TRUE;
  }

  // Don't add the Vivus on specified paths.
  if (!_vivus_ui_check_url()) {
    return TRUE;
  }

  // Attach Vivus.js to pages with chosen method.
  $method = vivus_check_installed() ? $config->get('method') : 'cdn';

  // Check for load development and production version.
  $variant_options = ['source', 'minified'];
  $variant = $variant_options[$config->get('minimized.options')];
  if ($method == 'cdn') {
    // Check variant to load Vivus from CDN.
    switch ($variant) {
      case 'minified':
        $attachments['#attached']['library'][] = 'vivus_ui/vivus-cdn';
        break;

      case 'source':
        $attachments['#attached']['library'][] = 'vivus_ui/vivus-cdn-dev';
        break;
    }
  }
  else {
    // Check variant to load Vivus from local.
    switch ($variant) {
      case 'minified':
        $attachments['#attached']['library'][] = 'vivus_ui/vivus-js';
        break;

      case 'source':
        $attachments['#attached']['library'][] = 'vivus_ui/vivus-dev';
        break;
    }
  }

  // Get stored vivus selector effects from database.
  $results = \Drupal::service('vivus.animation_manager')->loadVivus()->fetchAll();
  $effects = [];
  if (count($results)) {
    foreach ($results as $vivus) {
      $vivus_options = unserialize($vivus->options, ['allowed_classes' => FALSE]);
      $effects[$vivus->vid] = ['selector' => $vivus->selector] + $vivus_options;
    }
  }

  // If there is vivus selectors, then the init file
  // with options will be added to the page.
  if (count($effects)) {
    // Export settings.
    $attachments['#attached']['drupalSettings']['vivus']['records'] = $effects;

    // Attach and init vivus.
    $attachments['#attached']['library'][] = 'vivus_ui/vivus.init';
  }
}

/**
 * Check if vivus should be active for the current URL.
 *
 * @param \Symfony\Component\HttpFoundation\Request $request
 *   The request to use if provided, otherwise \Drupal::request() will be used.
 * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
 *   The request stack.
 *
 * @return bool
 *   TRUE if vivus should be active for the current page.
 */
function _vivus_ui_check_url(Request $request = NULL, RequestStack $request_stack = NULL) {
  if (!isset($request)) {
    $request = \Drupal::request();
  }

  // Assume there are no matches until one is found.
  $page_match = FALSE;

  // Make it possible deactivate letting with
  // parameter ?vivus=no in the url.
  $query = $request->query;
  if ($query->get('vivus') !== NULL && $query->get('vivus') == 'no') {
    return $page_match;
  }

  // Convert path to lowercase. This allows comparison of the same path
  // with different case. Ex: /Page, /page, /PAGE.
  $config = \Drupal::config('vivus.settings');
  $pages  = mb_strtolower(_vivus_ui_array_to_string($config->get('url.pages')));
  if (!$pages) {
    return TRUE;
  }
  if (!isset($request_stack)) {
    $request_stack = \Drupal::requestStack();
  }
  $current_request = $request_stack->getCurrentRequest();
  // Compare the lowercase path alias (if any) and internal path.
  $path = \Drupal::service('path.current')->getPath($current_request);
  // Do not trim a trailing slash if that is the complete path.
  $path = $path === '/' ? $path : rtrim($path, '/');
  $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();
  $path_alias = mb_strtolower(\Drupal::service('path_alias.manager')->getAliasByPath($path, $langcode));
  $page_match = \Drupal::service('path.matcher')->matchPath($path_alias, $pages);
  if ($path_alias != $path) {
    $page_match = $page_match || \Drupal::service('path.matcher')->matchPath($path, $pages);
  }
  $page_match = $config->get('url.visibility') == 0 ? !$page_match : $page_match;

  return $page_match;
}

/**
 * Converts a text with lines (\n) into an array of lines.
 *
 * @return array
 *   Array with as many items as non-empty lines in the text
 */
function _vivus_ui_string_to_array($text) {
  if (!is_string($text)) {
    return NULL;
  }
  $text = str_replace("\r\n", "\n", $text);
  return array_filter(explode("\n", $text), 'trim');
}

/**
 * Converts an array of lines into a text with lines (\n).
 *
 * @return string
 *   Text with lines
 */
function _vivus_ui_array_to_string($array) {
  if (!is_array($array)) {
    return NULL;
  }
  return implode("\r\n", $array);
}
