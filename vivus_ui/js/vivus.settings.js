/**
 * @file
 * Contains definition of the behaviour VivusAdmin.
 */

(function ($, Drupal, drupalSettings, once) {
  "use strict";

  Drupal.behaviors.vivusAdmin = {
    attach: function (context, settings) {

      if (once('vivus__preview', '.vivus__preview').length) {
        let $svgObject = $('body').find("object#svgObject")
          , $effectReplay = $('input.vivus__replay')
          , $effectRewind = $('input.vivus__rewind');

        var svgURL = $svgObject.attr('data');

        $svgObject.load(svgURL, function (data) {
          // Get the SVG tag, ignore the rest
          var $svg = $(this).find('svg');
          $('.vivus__preview').append($svg);
          $(this).remove();

          var vivusPreview;
          // Vivus preview replay.
          $effectReplay.bind('click', function (event) {
            let options = {
              type: $('.vivus-type').val(),
              start: $('.vivus-start').val(),
              duration: $('.vivus-duration').val(),
              delay: $('.vivus-delay').val(),
              dashGap: $('.vivus-dash-gap').val(),
              forceRender: $('.vivus-force-render').is(':checked'),
              reverseStack: $('.vivus-reverse-stack').is(':checked'),
              selfDestroy: $('.vivus-self-destroy').is(':checked'),
            };
            options['pathTimingFunction'] = Drupal.vivusGetTiming($('.vivus-path-timing').val());
            options['animTimingFunction'] = Drupal.vivusGetTiming($('.vivus-anim-timing').val());

            console.log(options);

            vivusPreview = new Vivus('polaroid', options);
            // The property 'map' contain all the SVG mapping.
            console.table(vivusPreview.map);

            event.preventDefault();
          }).trigger('click');;

          // Vivus preview replay.
          $effectRewind.bind('click', function (event) {
            setTimeout(function () {
              vivusPreview.play(-3);
            }, 10);
            event.preventDefault();
          });

        });
      }

      if (once('vivus__about', '.vivus__about').length) {
        new Drupal.vivusHelp();
      }

    }
  };

  Drupal.vivusHelp = function () {
    const welcome = drupalSettings.vivus.welcome;
    let vivusUI = new Vivus('vivusUI', {file: welcome, type: 'scenario-sync', duration: 20, start: 'autostart', dashGap: 20, forceRender: false},
        function () {
          if (window.console) {
            console.log('Animation finished. [log triggered from callback]');
            $('#hi-there').bind('click', function (event) {
              vivusUI.reset().play();
              event.preventDefault();
            });
          }
        });
  }

  /**
   * Get the current vivus timing.
   */
  Drupal.vivusGetTiming = function (timing) {
    var timingType;
    switch (timing) {
      case 'linear':
        timingType = Vivus.LINEAR;
        break;

      case 'ease':
        timingType = Vivus.EASE;
        break;

      case 'ease-in':
        timingType = Vivus.EASE_IN;
        break;

      case 'ease-out':
        timingType = Vivus.EASE_OUT;
        break;

      case 'ease-out-bounce':
        timingType = Vivus.EASE_OUT_BOUNCE;
        break;
    }
    return timingType;
  };

})(jQuery, Drupal, drupalSettings, once);
