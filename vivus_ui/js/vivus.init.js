/**
 * @file
 * Contains definition of the behaviour vivus initial.
 */

(function ($, Drupal, drupalSettings, once) {
  "use strict";

  Drupal.behaviors.vivusInit = {
    attach: function (context, settings) {

      const records = drupalSettings.vivus.records;

      $.each(records, function (index, record) {
        let svgID = '#' + record.selector;

        if (once(record.selector, svgID).length) {
          $(svgID).css({"display": "block", "visibility": "hidden"});
          $(svgID).addClass('vivusious');
          $(svgID).removeAttr('fill');
          $(svgID).removeAttr('stroke');

          Drupal.vivus(record.selector, record);
        }
      });

    }
  };

  /**
   * Vivus process function.
   */
  Drupal.vivus = function (id, record, count = 0) {
    let currentVivus
      , time = parseInt(record.displayTime);

    setTimeout( function () {
      let total = record.animations.length
        , animation = record.animations[count]
        , options = {
            start: record.start,
            dashGap: parseInt(record.dashGap),
            forceRender: Boolean(parseInt(record.forceRender)),
            selfDestroy: Boolean(parseInt(record.selfDestroy)),
          };

      options['type'] = animation.type;
      if (animation.duration) {
        options['duration'] = parseInt(animation.duration);
      }
      if (animation.delay) {
        options['delay'] = parseInt(animation.delay);
      }
      if (animation.reverseStack) {
        options['reverseStack'] = Boolean(parseInt(record.reverseStack));
      }
      if (animation.pathTimingFunction) {
        options['pathTimingFunction'] = Drupal.vivusTiming(animation.pathTimingFunction);
      }

      // Erase switch animation not working when selfDestroy is true.
      if (record.switch === "erase" && options['selfDestroy'] === true) {
        options['selfDestroy'] = false;
      }

      time = parseInt(record.displayTime);
      $('#' + id).css({"display": "block", "visibility": "visible"});
      currentVivus = new Vivus(id, options).play(function () {
        // Called after the animation completes.
        setTimeout( function () {
          count++;
          if (count < total) {
            switch (record.switch) {
              case 'erase':
                currentVivus.play(-1, function () {
                  Drupal.vivus(id, record, count);
                });
                break;

              case 'fade':
                $('#' + id).fadeOut( 600, "linear", function () {
                  $(this).css({"display": "block", "visibility": "hidden"});
                  Drupal.vivus(id, record, count);
                });
                break;
            }
          }
          else if (record.loop) {
            switch (record.switch) {
              case 'erase':
                currentVivus.play(-1, function () {
                  Drupal.vivus(id, record, 0);
                });
                break;

              case 'fade':
                $('#' + id).fadeOut( 600, "linear", function () {
                  $(this).css({"display": "block", "visibility": "hidden"});
                  Drupal.vivus(id, record, 0);
                });
                break;
            }
          }
        }, time);
      });

    }, parseInt(record.initialDelay));
  };

  /**
   * Get the current vivus timing.
   */
  Drupal.vivusTiming = function (timing) {
    let timingType;
    switch (timing) {
      case 'linear':
        timingType = Vivus.LINEAR;
        break;

      case 'ease':
        timingType = Vivus.EASE;
        break;

      case 'ease-in':
        timingType = Vivus.EASE_IN;
        break;

      case 'ease-out':
        timingType = Vivus.EASE_OUT;
        break;

      case 'ease-out-bounce':
        timingType = Vivus.EASE_OUT_BOUNCE;
        break;
    }
    return timingType;
  };

})(jQuery, Drupal, drupalSettings, once);
