/**
 * @file
 * Vivus form behaviors.
 */

(function ($, Drupal, drupalSettings, once) {

  "use strict";

  Drupal.behaviors.vivusForm = {
    attach: function (context, settings) {

      var $context = $(context);
      $context.find('#edit-options').drupalSetSummary(function (context) {
        var values = [];
        if ($(context).find('#edit-loop').is(':checked')) {
          values.unshift(Drupal.t('Loop'));
        }
        var startMode = $(context).find('#edit-start option:selected').text();
        values.push(Drupal.t('@startMode', {'@startMode': startMode}));
        var changeAnim = $(context).find('#edit-switch').val();
        if (changeAnim) {
          var switchMode = $(context).find('#edit-switch option:selected').text();
          values.push(Drupal.t('@switchMode', {'@switchMode': switchMode}));
        }
        var initialDelay = Drupal.checkPlain($(context).find('#edit-initial-delay')[0].value) || 0;
        values.push(Drupal.t('Initial delay: @initialDelay', {'@initialDelay': initialDelay}));
        var displayTime = Drupal.checkPlain($(context).find('#edit-display-time')[0].value) || 0;
        values.push(Drupal.t('Display time: @displayTime', {'@displayTime': displayTime}));
        return values.join(', ');
      });
      $context.find('#edit-advanced').drupalSetSummary(function (context) {
        var values = []
          , eventType = $(context).find('#edit-event option:selected').text();
        values.push(Drupal.t('Javascript event: @eventType', {'@eventType': eventType}));
        var dashGap = Drupal.checkPlain($(context).find('#edit-dash-gap')[0].value) || 0;
        values.push(Drupal.t('Dash gap: @dashGap', {'@dashGap': dashGap}));
        if ($(context).find('#edit-force-render').is(':checked')) {
          values.unshift(Drupal.t('Force render'));
        }
        if ($(context).find('#edit-self-destroy').is(':checked')) {
          values.unshift(Drupal.t('Self destroy'));
        }
        return values.join(', ');
      });
      $context.find('#edit-administration').drupalSetSummary(function (context) {
        var values = [];
        if ($(context).find('#edit-status').is(':checked')) {
          values.unshift(Drupal.t('Enabled'));
        } else {
          values.unshift(Drupal.t('Disabled'));
        }
        return values.join(', ');
      });

      $(once('vivus-form-tr', '#vivus-form tr', context)).each(function (index) {

        let currentRow = $(this)
          , preview = currentRow.find('div.vivus-preview')
          , select = currentRow.find('div.form-item select.vivus-type')
          , cancel = currentRow.find('input.vivus-cancel')
          , update = currentRow.find('input.vivus-update')
          , setting = currentRow.find('input.vivus-edit')
          , options = currentRow.find('div.vivus-options');

        if (options.closest('td').length) {
          options.closest('td').hide();
        }

        select.bind('change', Drupal.vivusPreview);

        preview.bind({
          click: function (e) {
            // Click event handler.
            select.trigger('change');
          },
        });

        let duration = currentRow.find('.vivus-duration').val()
          , delay = currentRow.find('.vivus-delay').val()
          , path = currentRow.find('.vivus-path').val()
          , reverse = currentRow.find('.vivus-reverse').is(':checked');

        setting.bind({
          click: function (e) {
            Drupal.vivusShowColumns($(this));
            e.preventDefault();
          },
        });

        cancel.bind({
          click: function (e) {
            Drupal.vivusHideColumns($(this));
            currentRow.find('.vivus-duration').val(duration);
            currentRow.find('.vivus-delay').val(delay);
            currentRow.find('.vivus-path').val(path);
            currentRow.find('.vivus-reverse').is(':checked');
            e.preventDefault();
          },
        });

        update.bind({
          click: function (e) {
            Drupal.vivusHideColumns($(this));
            duration = currentRow.find('.vivus-duration').val();
            delay = currentRow.find('.vivus-delay').val() !== '' ? currentRow.find('.vivus-delay').val() : 0;
            path = currentRow.find('.vivus-path').val();
            reverse = currentRow.find('.vivus-reverse').is(':checked') ? Drupal.t('True') : Drupal.t('False');

            let pathTxt = currentRow.find('.vivus-path option:selected').text();
            let summary = Drupal.t('Duration: @duration', {'@duration': duration}) + "<br>"
                        + Drupal.t('Delay: @delay', {'@delay': delay}) + "<br>"
                        + Drupal.t('Reverse stack: @reverse', {'@reverse': reverse}) + "<br>"
                        + Drupal.t('Path timing: @path', {'@path': pathTxt}) + "<br>"

            currentRow.find('.vivus-summary').html(summary);

            // Preview event handler.
            select.trigger('change');
            e.preventDefault();
          },
        });

      });

      $('#edit-effects-more').bind({
        mousedown: function (e) {
          let selectorLabel = $('.form-item--selector label');
          let selectorField = $('.form-item--selector input');
          if (selectorField.val().trim() === '') {
            selectorLabel.addClass('has-error');
            selectorField.addClass('error');
          }
          else {
            if (selectorLabel.hasClass('has-error')) {
              selectorLabel.removeClass('has-error');
              selectorField.removeClass('error');
            }
          }
          e.preventDefault();
        },
      });

    }
  };

  /**
   * Shows/hides options forms for various vivus.
   *
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the show/hide functionality to each table row in the vivus edit form.
   */
  Drupal.vivusPreview = function () {
    let animation = $(this).closest('tr');
    let options = {
      type: animation.find('.vivus-type').val(),
      start: animation.find('.vivus-start').val(),
      duration: animation.find('.vivus-duration').val(),
      delay: animation.find('.vivus-delay').val(),
      reverseStack: animation.find('.vivus-reverse').is(':checked'),
      dashGap: animation.find('.vivus-dash-gap').val(),
      forceRender: animation.find('.vivus-force-render').is(':checked'),
      selfDestroy: animation.find('.vivus-self-destroy').is(':checked'),
    };
    options['pathTimingFunction'] = Drupal.vivusGetTiming(animation.find('.vivus-path').val());

    setTimeout(function () {
      const vivusPreview = new Vivus(animation.find('.shutter').attr('id'), options);
      // The property 'map' contain all the SVG mapping.
      console.table(vivusPreview.map);
    }, 10);

  };

  /**
   * Get the current vivus timing.
   */
  Drupal.vivusGetTiming = function (timing) {
    var timingType;
    switch (timing) {
      case 'linear':
        timingType = Vivus.LINEAR;
        break;

      case 'ease':
        timingType = Vivus.EASE;
        break;

      case 'ease-in':
        timingType = Vivus.EASE_IN;
        break;

      case 'ease-out':
        timingType = Vivus.EASE_OUT;
        break;

      case 'ease-out-bounce':
        timingType = Vivus.EASE_OUT_BOUNCE;
        break;
    }
    return timingType;
  };

  /**
   * Hide the columns containing weight/parent form elements.
   *
   * Undo showColumns().
   */
  Drupal.vivusHideColumns = function (cancel) {
    const $tables = cancel.closest('tr');
    $tables.removeClass('field-plugin-settings-editing');
    // Show weight/parent cells and headers.
    $tables.find('.vivus-edit').closest('td').show();
    $tables.find('.vivus-delete').closest('td').show();
    $tables.find('.vivus-options').closest('td').hide();
    $('.th__').show();
  };

  /**
   * Show the columns containing weight/parent form elements.
   *
   * Undo hideColumns().
   */
  Drupal.vivusShowColumns = function (setting) {
    const $tables = setting.closest('tr');
    $tables.addClass('field-plugin-settings-editing');
    // Show weight/parent cells and headers.
    setting.closest('td').hide();
    $tables.find('.vivus-delete').closest('td').hide();
    $tables.find('.vivus-options').closest('td').show();
    $('.th__').hide();
  };

})(jQuery, Drupal, drupalSettings, once);
