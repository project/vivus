<?php

namespace Drupal\vivus_ui;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\vivus_ui\Form\VivusFilter;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Displays list of Vivus animations.
 *
 * @internal
 */
class VivusAdmin extends FormBase {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Vivus manager.
   *
   * @var \Drupal\vivus_ui\VivusManagerInterface
   */
  protected $vivusManager;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * Constructs a new Animate object.
   *
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   * @param \Drupal\vivus_ui\VivusManagerInterface $vivus_manager
   *   The Animate selector manager.
   * @param \Symfony\Component\HttpFoundation\Request $current_request
   *   The current request.
   */
  public function __construct(DateFormatterInterface $date_formatter, FormBuilderInterface $form_builder, VivusManagerInterface $vivus_manager, Request $current_request) {
    $this->dateFormatter = $date_formatter;
    $this->formBuilder = $form_builder;
    $this->vivusManager = $vivus_manager;
    $this->currentRequest = $current_request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('date.formatter'),
      $container->get('form_builder'),
      $container->get('vivus.animation_manager'),
      $container->get('request_stack')->getCurrentRequest(),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vivus_admin_form';
  }

  /**
   * {@inheritdoc}
   *
   * @param array $form
   *   A nested array form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Attach Vivus overview admin library.
    $form['#attached']['library'][] = 'vivus_ui/vivus.list';

    $search = $this->currentRequest->query->get('search');
    $status = $this->currentRequest->query->get('status') ?? NULL;

    /** @var \Drupal\vivus_ui\Form\VivusFilter $form */
    $form['vivus_admin_filter_form'] = $this->formBuilder->getForm(VivusFilter::class, $search, $status);
    $form['#attributes']['class'][] = 'vivus-filter';
    $form['#attributes']['class'][] = 'views-exposed-form';

    $header = [
      [
        'data'  => $this->t('Selector'),
        'field' => 'v.vid',
      ],
      [
        'data'  => $this->t('Label'),
        'field' => 'v.label',
      ],
      [
        'data'  => $this->t('Status'),
        'field' => 'v.status',
      ],
      [
        'data'  => $this->t('Updated'),
        'field' => 'v.changed',
        'sort'  => 'desc',
      ],
      $this->t('Operations'),
    ];

    $rows = [];
    $result = $this->vivusManager->findAll($header, $search, $status);
    foreach ($result as $vivus) {
      $row = [];
      $row['selector'] = $vivus->selector;
      $row['label'] = $vivus->label;
      $status_class = $vivus->status ? 'marker marker--enabled' : 'marker';
      $row['status'] = [
        'data' => [
          '#type' => 'markup',
          '#prefix' => '<span class="' . $status_class . '">',
          '#suffix' => '</span>',
          '#markup' => $vivus->status ? $this->t('Enabled') : $this->t('Disabled'),
        ],
      ];
      $row['update'] = $this->dateFormatter->format($vivus->changed, 'short');
      $links = [];
      $links['edit'] = [
        'title' => $this->t('Edit'),
        'url'   => Url::fromRoute('vivus.edit', ['vid' => $vivus->vid]),
      ];
      $links['delete'] = [
        'title' => $this->t('Delete'),
        'url'   => Url::fromRoute('vivus.delete', ['vid' => $vivus->vid]),
      ];
      $links['duplicate'] = [
        'title' => $this->t('Duplicate'),
        'url'   => Url::fromRoute('vivus.duplicate', ['vid' => $vivus->vid]),
      ];
      $row[] = [
        'data' => [
          '#type'  => 'operations',
          '#links' => $links,
        ],
      ];
      $rows[] = $row;
    }

    $form['vivus_admin_table'] = [
      '#type'   => 'table',
      '#header' => $header,
      '#rows'   => $rows,
      '#empty'  => $this->t('No vivus svg animation available. <a href=":link">Add animation</a> with vivus.', [
        ':link' => Url::fromRoute('vivus.add')
          ->toString(),
      ]),
      '#attributes' => ['class' => ['vivus-list']],
    ];

    $form['pager'] = ['#type' => 'pager'];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // @todo Add operations to vivus list
  }

}
