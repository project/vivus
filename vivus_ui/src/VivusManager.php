<?php

namespace Drupal\vivus_ui;

use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\PagerSelectExtender;
use Drupal\Core\Database\Query\TableSortExtender;

/**
 * Vivus manager.
 */
class VivusManager implements VivusManagerInterface {

  /**
   * The database connection used to check the selector against.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Constructs a VivusManager object.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection which will be used to check the selector
   *   against.
   */
  public function __construct(Connection $connection) {
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public function isVivus($selector) {
    return (bool) $this->connection->query("SELECT * FROM {vivus} WHERE [selector] = :selector", [':selector' => $selector])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function loadVivus() {
    $query = $this->connection
      ->select('vivus', 'v')
      ->fields('v', ['vid', 'selector', 'options'])
      ->condition('status', 1);

    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function addVivus($vivus_id, $selector, $label, $comment, $changed, $status, $options) {
    $this->connection->merge('vivus')
      ->key('vid', $vivus_id)
      ->fields([
        'selector' => $selector,
        'label'    => $label,
        'comment'  => $comment,
        'changed'  => $changed,
        'status'   => $status,
        'options'  => $options,
      ])
      ->execute();

    return $this->connection->lastInsertId();
  }

  /**
   * {@inheritdoc}
   */
  public function removeVivus($vivus_id) {
    $this->connection->delete('vivus')
      ->condition('vid', $vivus_id)
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function findAll($header = [], $search = '', $status = NULL) {
    $query = $this->connection
      ->select('vivus', 'v')
      ->extend(PagerSelectExtender::class)
      ->extend(TableSortExtender::class)
      ->orderByHeader($header)
      ->limit(50)
      ->fields('v');

    if (!empty($search) && !empty(trim((string) $search)) && $search !== NULL) {
      $search = trim((string) $search);
      // Escape for LIKE matching.
      $search = $this->connection->escapeLike($search);
      // Replace wildcards with MySQL/PostgreSQL wildcards.
      $search = preg_replace('!\*+!', '%', $search);
      // Add selector and the label field columns.
      $group = $query->orConditionGroup()
        ->condition('selector', '%' . $search . '%', 'LIKE')
        ->condition('label', '%' . $search . '%', 'LIKE');
      // Run the query to find matching targets.
      $query->condition($group);
    }

    // Check if status is set.
    if (!is_null($status) && $status != '') {
      $query->condition('status', $status);
    }

    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function findById($vivus_id) {
    return $this->connection->query("SELECT [selector], [label], [comment], [status], [options] FROM {vivus} WHERE [vid] = :vid", [':vid' => $vivus_id])
      ->fetchAssoc();
  }

}
