<?php

namespace Drupal\vivus_ui\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\vivus_ui\VivusManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Vivus add and edit effect form.
 *
 * @internal
 */
class VivusForm extends FormBase {

  /**
   * Animate manager.
   *
   * @var \Drupal\vivus_ui\VivusManagerInterface
   */
  protected $vivusManager;

  /**
   * A config object for the system performance animation.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs a new vivus object.
   *
   * @param \Drupal\vivus_ui\VivusManagerInterface $vivus_manager
   *   The vivus selector manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   A config factory for retrieving required config objects.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(VivusManagerInterface $vivus_manager, ConfigFactoryInterface $config_factory, TimeInterface $time) {
    $this->vivusManager = $vivus_manager;
    $this->config = $config_factory->get('vivus.settings');
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('vivus.animation_manager'),
      $container->get('config.factory'),
      $container->get('datetime.time'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vivus_form';
  }

  /**
   * {@inheritdoc}
   *
   * @param array $form
   *   A nested array form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param int $vid
   *   (optional) Animate id to be passed on to
   *   \Drupal::formBuilder()->getForm() for use as the default value of the
   *   Animate ID form data.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $vid = 0) {
    // Vivus flags for specific form labels and suffix.
    $experimental_label = ' <span class="vivus-experimental-flag">Experimental</span>';
    $beta_label = ' <span class="vivus-beta-flag">Beta</span>';
    $new_label = ' <span class="vivus-new-flag">New</span>';
    $ms_unit_label = ' <span class="vivus-unit-flag">ms</span>';
    $px_unit_label = ' <span class="vivus-unit-flag">px</span>';

    // Attach vivus form library.
    $form['#attached']['library'][] = 'vivus_ui/vivus.form';

    // Load the Vivus configuration settings.
    $config = $this->config;

    // Prepare Vivus form default values.
    $vivus_id = $vid;
    $vivus    = $this->vivusManager->findById($vivus_id) ?? [];
    $selector = $vivus['selector'] ?? '';
    $label    = $vivus['label'] ?? '';
    $comment  = $vivus['comment'] ?? '';
    $status   = $vivus['status'] ?? TRUE;
    $options  = [];

    // Handle the case when $vivus is not an array or option is not set.
    if (is_array($vivus) && isset($vivus['options'])) {
      $options = unserialize($vivus['options'], ['allowed_classes' => FALSE]) ?? '';
    }

    // Store vivus id.
    $form['vivus_id'] = [
      '#type'  => 'value',
      '#value' => $vivus_id,
    ];

    // The default selector to use when detecting multiple svg to animate.
    $form['selector'] = [
      '#title'         => $this->t('ID'),
      '#type'          => 'textfield',
      '#required'      => TRUE,
      '#size'          => 64,
      '#maxlength'     => 255,
      '#default_value' => $selector,
      '#description'   => $this->t('Here, you can use svg ID without hash(#) prefix. Make sure your selector has plain text content. e.g. ".page-title" or ".block-title".'),
      '#placeholder'   => $this->t('Enter valid selector'),
    ];

    // The label of this selector.
    $form['label'] = [
      '#title'         => $this->t('Label'),
      '#type'          => 'textfield',
      '#required'      => FALSE,
      '#size'          => 64,
      '#maxlength'     => 255,
      '#default_value' => $label ?? '',
      '#description'   => $this->t('Enter a short label for this selector. The label will be displayed on the Vivus effects administration overview page.'),
    ];

    // Create a table with each <tr> corresponding to an effect.
    $vivus = new \stdClass();
    if (!empty($form_state->getValue('animation_data'))) {
      $animations = [];
      foreach ($form_state->getValue('animation_data') as $index => $animation) {
        $animations[$index] = [
          'type'               => $animation['animation_type'],
          'duration'           => $animation['options_wrapper']['duration'],
          'delay'              => $animation['options_wrapper']['delay'],
          'pathTimingFunction' => $animation['options_wrapper']['path_timing'],
          'reverseStack'       => $animation['options_wrapper']['reverse_stack'],
          'weight'             => $animation['weight'],
        ];
      }
      $vivus->animations = $animations;
    }
    else {
      $vivus->animations = $options['animations'] ?? [];
    }

    // Show an empty animation when adding a new vivus.
    if (empty($vivus->animations)) {
      $vivus->animations = [
        0 => [],
      ];
    }
    else {
      if (is_numeric($form_state->get('to_remove'))) {
        unset($vivus->animations[$form_state->get('to_remove')]);
        $form_state->set('num_effects', $form_state->get('num_effects') - 1);
        $form_state->set('to_remove', NULL);
      }

      // If the number of rows has been incremented add another row.
      if ($form_state->get('num_effects') > count($vivus->animations)) {
        $vivus->animations[] = [];
      }
    }

    $form['animation_data_wrapper'] = [
      '#tree'   => FALSE,
      '#prefix' => '<div class="clear-block" id="animation-data-wrapper">',
      '#suffix' => '</div>',
    ];
    $form['animation_data_wrapper']['animation_data'] = $this->getAnimationDataForm($vivus, $config);

    // There are two functions attached to the more button:
    // The submit function will be called first; increments number of rows.
    // The callback function will then return the rendered rows.
    $form['effects_more'] = [
      '#name'       => 'effects_more',
      '#type'       => 'submit',
      '#value'      => $this->t('Add more'),
      '#attributes' => [
        'class' => ['add-effect'],
        'title' => $this->t('Click here to add more animations.'),
      ],
      '#submit'     => [[$this, 'ajaxFormSubmit']],
      '#ajax'       => [
        'callback' => [$this, 'ajaxFormCallback'],
        'progress' => [
          'type'    => 'throbber',
          'message' => NULL,
        ],
        'effect'   => 'fade',
      ],
    ];

    // Vertical tabs for settings.
    $form['settings'] = [
      '#type'    => 'vertical_tabs',
      '#parents' => ['settings'],
      '#tree'    => TRUE,
    ];

    // Vivus main options.
    $form['options'] = [
      '#type'       => 'details',
      '#title'      => $this->t('Vivus settings'),
      '#attributes' => ['class' => ['details--settings', 'b-tooltip']],
      '#group'      => 'settings',
      '#open'       => TRUE,
    ];

    // Enable animation looping.
    $form['options']['loop'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Loop') . $beta_label,
      '#default_value' => $options['loop'] ?? $config->get('additional.loop'),
      '#description'   => $this->t("If enabled, element will repeat the animation."),
      '#attributes'    => ['class' => ['vivus-loop']],
    ];

    // Set whether to automatically start animating.
    $form['options']['start'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Start'),
      '#options'       => vivus_start_options(),
      '#default_value' => $options['start'] ?? $config->get('options.start'),
      '#description'   => $this->t('Defines how to trigger the animation (inViewport once the SVG is in the viewport, manual gives you the freedom to call draw method to start, autostart makes it start right now).'),
      '#attributes'    => ['class' => ['vivus-start']],
    ];

    // Switch method between the animations.
    $form['options']['switch'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Switch animation') . $new_label . $experimental_label,
      '#options'       => vivus_switch_options(),
      '#default_value' => $options['switch'] ?? $config->get('additional.switch'),
      '#description'   => $this->t('Defines how animations change when a loop is enabled or has more than one animation (Default once the SVG will replace by another animation draw, Erase gives you rewind draw animation, Fade out makes disappear to start next animation).'),
      '#attributes'    => ['class' => ['vivus-switch']],
    ];

    // Whitespace extra margin between dashes.
    $form['options']['initial_delay'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Initial delay'),
      '#size'          => 6,
      '#maxlength'     => 9,
      '#default_value' => $options['initialDelay'] ?? $config->get('additional.initialDelay'),
      '#description'   => $this->t('Sets the initial delay before the animation starts.'),
      '#attributes'    => ['class' => ['vivus-initial-delay']],
      '#field_suffix'  => $ms_unit_label,
    ];

    // Whitespace extra margin between dashes.
    $form['options']['display_time'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Display time'),
      '#size'          => 6,
      '#maxlength'     => 9,
      '#default_value' => $options['displayTime'] ?? $config->get('additional.displayTime'),
      '#description'   => $this->t('Sets the minimum display time for each animation before replacing.'),
      '#attributes'    => ['class' => ['vivus-display-time']],
      '#field_suffix'  => $ms_unit_label,
    ];

    // Vivus advanced options.
    $form['advanced'] = [
      '#type'       => 'details',
      '#title'      => $this->t('Advanced options'),
      '#attributes' => ['class' => ['details--advanced', 'b-tooltip']],
      '#group'      => 'settings',
    ];

    // Force the browser to re-render all updated path items.
    $form['advanced']['force_render'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Force render'),
      '#default_value' => $options['forceRender'] ?? $config->get('options.forceRender'),
      '#description'   => $this->t("Force the browser to re-render all updated path items. By default, the value is true on IE only."),
      '#attributes'    => ['class' => ['vivus-force-render']],
    ];

    // Removes all extra styling on the SVG, and leaves it as original.
    $form['advanced']['self_destroy'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Self destroy'),
      '#default_value' => $options['selfDestroy'] ?? $config->get('options.selfDestroy'),
      '#description'   => $this->t("Removes all extra styling on the SVG, and leaves it as original."),
      '#attributes'    => ['class' => ['vivus-self-destroy']],
    ];

    // Whitespace extra margin between dashes.
    $form['advanced']['dash_gap'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Dash gap'),
      '#size'          => 1,
      '#maxlength'     => 3,
      '#default_value' => $options['dashGap'] ?? $config->get('options.dashGap'),
      '#description'   => $this->t('Whitespace extra margin between dashes. Increase it in case of glitches at the initial state of the animation.'),
      '#attributes'    => ['class' => ['vivus-dash-gap']],
      '#field_suffix'  => $px_unit_label,
    ];

    // Vivus administration tab.
    $form['administration'] = [
      '#type'       => 'details',
      '#title'      => $this->t('Administration'),
      '#attributes' => ['class' => ['details--administration', 'b-tooltip']],
      '#group'      => 'settings',
    ];

    // Enabled status for this vivus effects.
    $form['administration']['status'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Enabled'),
      '#description'   => $this->t('Effects will appear on pages that have this selector.'),
      '#default_value' => $status ?? TRUE,
    ];

    // The comment for describe animate settings and usage in website.
    $form['administration']['help'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('Explanation guidelines'),
      '#default_value' => $comment ?? '',
      '#description'   => $this->t('Describe this animation settings and usage in your website. The text will be displayed only on this form as comment for administration section.'),
      '#rows'          => 3,
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type'        => 'submit',
      '#value'       => $this->t('Save'),
      '#button_type' => 'primary',
      '#submit'      => [[$this, 'submitForm']],
    ];

    if ($vid != 0) {
      // Add a 'Remove' button for effects form.
      $form['actions']['delete'] = [
        '#type'       => 'link',
        '#title'      => $this->t('Delete'),
        '#url'        => Url::fromRoute('vivus.delete', ['vid' => $vid]),
        '#attributes' => [
          'class' => [
            'action-link',
            'action-link--danger',
            'action-link--icon-trash',
          ],
        ],
      ];

      // Redirect to list for submit handler on edit form.
      $form['actions']['submit']['#submit'] = ['::submitForm', '::overview'];
    }
    else {
      // Add a 'Save and go to list' button for add form.
      $form['actions']['overview'] = [
        '#type'   => 'submit',
        '#value'  => $this->t('Save and go to list'),
        '#submit' => array_merge($form['actions']['submit']['#submit'], ['::overview']),
        '#weight' => 20,
      ];
    }

    return $form;
  }

  /**
   * Ajax callback for the add tab and remove tab buttons.
   *
   * Returns the table rows.
   */
  public function ajaxFormCallback(array &$form, FormStateInterface $form_state) {
    // Clear selector validate error message via ajax.
    if ($form_state->getErrors()) {
      $this->messenger()->deleteAll();
    }

    // Instantiate an AjaxResponse Object to return.
    $ajax_response = new AjaxResponse();
    $ajax_response->addCommand(new HtmlCommand('#animation-data-wrapper', $form['animation_data_wrapper']['animation_data']));

    return $ajax_response;
  }

  /**
   * Submit handler for the 'Add Effect' and 'Remove' buttons.
   *
   * Removes a row or increments the number of rows depending on action.
   */
  public function ajaxFormSubmit(array &$form, FormStateInterface $form_state) {
    if ($form_state->getTriggeringElement()['#name'] === 'effects_more') {
      $form_state->set('num_effects', count($form_state->getValue('animation_data')) + 1);
      $form_state->setRebuild(TRUE);
    }
    elseif (is_numeric($form_state->getTriggeringElement()['#row_number'])) {
      $form_state->set('to_remove', $form_state->getTriggeringElement()['#row_number']);
      $form_state->setRebuild(TRUE);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $vid      = $form_state->getValue('vivus_id');
    $is_new   = $vid == 0;
    $selector = trim($form_state->getValue('selector'));

    if ($is_new) {
      if ($this->vivusManager->isVivus($selector)) {
        $form_state->setErrorByName('selector', $this->t('This ID is already exists.'));
      }
    }
    else {
      if ($this->vivusManager->findById($vid)) {
        $vivus = $this->vivusManager->findById($vid);

        if ($selector != $vivus['selector'] && $this->vivusManager->isVivus($selector)) {
          $form_state->setErrorByName('selector', $this->t('This ID is already added.'));
        }
      }
    }

    // Check duration and delay options.
    foreach ($form_state->getValue('animation_data') as $index => $animation) {
      if ($animation['options_wrapper']['duration'] < $animation['options_wrapper']['delay']) {
        $form_state->setErrorByName("animation_data[$index][options_wrapper][delay]", $this->t('Vivus [constructor]: delay must be shorter than duration.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values    = $form_state->getValues();
    $variables = [];

    $vid      = $values['vivus_id'];
    $label    = trim($values['label']);
    $selector = trim($values['selector']);
    $comment  = trim($values['help']);
    $status   = $values['status'];

    // Provide a label from selector if was empty.
    if (empty($label)) {
      $label = ucfirst(trim(preg_replace("/[^a-zA-Z0-9]+/", " ", $selector)));
    }

    // Animation effect and options.
    foreach ($values['animation_data'] as $index => $animation) {
      $variables['animations'][$index] = [
        'type'               => $animation['animation_type'],
        'duration'           => $animation['options_wrapper']['duration'],
        'delay'              => $animation['options_wrapper']['delay'],
        'pathTimingFunction' => $animation['options_wrapper']['path_timing'],
        'reverseStack'       => $animation['options_wrapper']['reverse_stack'],
        'weight'             => $animation['weight'],
      ];
    }

    // Vivus main settings.
    $variables['loop']         = $values['loop'];
    $variables['start']        = $values['start'];
    $variables['switch']       = $values['switch'];
    $variables['initialDelay'] = $values['initial_delay'];
    $variables['displayTime']  = $values['display_time'];

    // Advanced options data.
    $variables['forceRender'] = $values['force_render'];
    $variables['selfDestroy'] = $values['self_destroy'];
    $variables['dashGap']     = $values['dash_gap'];

    // Serialize options variables.
    $options = serialize($variables);

    // The Unix timestamp when the vivus was most recently saved.
    $changed = $this->time->getCurrentTime();

    // Vivus save.
    $this->vivusManager->addVivus($vid, $selector, $label, $comment, $changed, $status, $options);
    $this->messenger()
      ->addStatus($this->t('The svg ID %selector has been added.', ['%selector' => $selector]));

    // Flush caches so the updated config can be checked.
    drupal_flush_all_caches();
  }

  /**
   * Submit handler for removing vivus.
   *
   * @param array[] $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function remove(array &$form, FormStateInterface $form_state) {
    $vid = $form_state->getValue('vivus_id');
    $form_state->setRedirect('vivus.delete', ['vid' => $vid]);
  }

  /**
   * Form submission handler for the 'overview' action.
   *
   * @param array[] $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function overview(array $form, FormStateInterface $form_state): void {
    $form_state->setRedirect('vivus.admin');
  }

  /**
   * Returns animation data form.
   */
  private function getAnimationDataForm($vivus, $config) {
    $animation_data = [
      '#type'       => 'table',
      '#header'     => [
        ['data' => $this->t('Animation type')],
        ['data' => $this->t('Weight')],
        ['data' => $this->t('Preview')],
        ['data' => $this->t('Options')],
        ['data' => $this->t('Operations')],
      ],
      '#empty'      => $this->t('There are no animation yet.'),
      '#tabledrag'  => [
        [
          'action'       => 'order',
          'relationship' => 'sibling',
          'group'        => 'animations-order-weight',
        ],
      ],
      '#attributes' => ['class' => ['svg-animation']],
    ];

    foreach ($vivus->animations as $index => $animation) {
      $animation['delta']       = $index;
      $animation['animation']   = $animation['type'] ?? $config->get('options.animation');
      $animation['duration']    = $animation['duration'] ?? $config->get('options.duration');
      $animation['delay']       = $animation['delay'] ?? $config->get('options.delay');
      $animation['reverse']     = $animation['reverseStack'] ?? $config->get('options.reverseStack');
      $animation['path_timing'] = $animation['pathTimingFunction'] ?? $config->get('options.pathTimingFunction');
      $animation['summary']     = $this->optionsSummary($animation);
      $animation['weight']      = $animation['weight'] ?? $index;
      $animation_data[$index]   = $this->getRow($index, $animation);
    }

    return $animation_data;
  }

  /**
   * Builds and returns instance row.
   */
  private function getRow($row_number, $animation = NULL) {
    if ($animation === NULL) {
      $animation = [];
    }

    $row = [];
    // TableDrag: Mark the table row as draggable.
    $row['#attributes']['class'][] = 'draggable';
    // TableDrag: Sort the table row according to its configured weight.
    $row['#weight'] = $animation['weight'] ?? 0;

    // The animation effect name to use in.
    $row['animation_type'] = [
      '#title'         => $this->t('Animation type'),
      '#title_display' => 'invisible',
      '#type'          => 'select',
      '#options'       => vivus_type_options(),
      '#default_value' => $animation['animation'],
      '#attributes'    => ['class' => ['vivus-type']],
    ];

    // TableDrag: Weight column element.
    $row['weight'] = [
      '#type'          => 'weight',
      '#title'         => $this->t('Weight'),
      '#title_display' => 'invisible',
      '#default_value' => $animation['weight'] ?? 0,
      '#attributes'    => ['class' => ['animations-order-weight']],
    ];

    // Vivus svg animation preview.
    $row['preview'] = [
      '#markup' => '<svg id="shutter' . $animation['delta'] . '" class="shutter" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="100%" height="200px" viewBox="0 0 200 200" enable-background="new 0 0 200 200">
        <circle cx="100" cy="100" r="90"/>
        <circle cx="100" cy="100" r="85.74"/>
        <circle cx="100" cy="100" r="72.947"/>
        <circle cx="100" cy="100" r="39.74"/>
        <line x1="34.042" y1="131.189" x2="67.047" y2="77.781"/>
        <line x1="31.306" y1="75.416" x2="92.41" y2="60.987"/>
        <line x1="68.81" y1="34.042" x2="122.219" y2="67.046"/>
        <line x1="124.584" y1="31.305" x2="139.013" y2="92.409"/>
        <line x1="165.957" y1="68.809" x2="132.953" y2="122.219"/>
        <line x1="168.693" y1="124.584" x2="107.59" y2="139.012"/>
        <line x1="131.19" y1="165.957" x2="77.781" y2="132.953"/>
        <line x1="75.417" y1="168.693" x2="60.987" y2="107.59"/>
      </svg>',
      '#prefix' => '<div class="vivus-preview">',
      '#suffix' => '</div>',
      '#allowed_tags' => ['div', 'svg', 'circle', 'line', 'path'],
    ];

    // Settings wrapper.
    $row['settings_wrapper'] = [
      '#type'       => 'container',
      '#attributes' => ['class' => ['vivus-settings', 'container-inline']],
    ];

    // Edit settings cog icon button.
    $row['settings_wrapper']['settings_edit'] = [
      '#type'       => 'image_button',
      '#name'       => 'settings_edit',
      '#src'        => 'core/misc/icons/787878/cog.svg',
      '#op'         => 'edit',
      '#prefix'     => '<div class="vivus-settings-edit-wrapper">',
      '#suffix'     => '</div>',
      '#attributes' => [
        'class' => ['vivus-edit'],
        'alt'   => $this->t('Edit'),
      ],
    ];

    // Effect settings summary.
    $row['settings_wrapper']['settings_summary'] = [
      '#type'            => 'inline_template',
      '#template'        => '<div class="vivus-summary field-plugin-summary">{{ summary|safe_join("<br />") }}</div>',
      '#context'         => ['summary' => $animation['summary']],
      '#cell_attributes' => ['class' => ['field-plugin-summary-cell']],
    ];

    // Effect options wrapper.
    $row['options_wrapper'] = [
      '#type'       => 'container',
      '#attributes' => ['class' => ['vivus-options', 'container-inline']],
    ];

    // The delay factor applied to each consecutive character.
    $row['options_wrapper']['duration'] = [
      '#title'         => $this->t('Duration'),
      '#type'          => 'number',
      '#size'          => 3,
      '#maxlength'     => 6,
      '#default_value' => $animation['duration'],
      '#attributes'    => ['class' => ['vivus-duration']],
    ];

    // Set the delay between each character.
    $row['options_wrapper']['delay'] = [
      '#title'         => $this->t('Delay'),
      '#type'          => 'number',
      '#size'          => 3,
      '#maxlength'     => 6,
      '#default_value' => $animation['delay'],
      '#attributes'    => ['class' => ['vivus-delay']],
    ];

    // Set whether to automatically start animating.
    $row['options_wrapper']['path_timing'] = [
      '#title'         => $this->t('Path timing'),
      '#type'          => 'select',
      '#options'       => vivus_timing_options(),
      '#default_value' => $animation['path_timing'],
      '#attributes'    => ['class' => ['vivus-path']],
    ];

    // Reverse the order of execution.
    $row['options_wrapper']['reverse_stack'] = [
      '#title'         => $this->t('Reverse stack'),
      '#type'          => 'checkbox',
      '#default_value' => $animation['reverse'],
      '#attributes'    => ['class' => ['vivus-reverse']],
    ];

    // Effect option operations container.
    $row['options_operations'] = [
      '#type'       => 'container',
      '#attributes' => ['class' => ['vivus-options', 'container-inline']],
    ];

    // Effect update button.
    $row['options_operations']['settings_update'] = [
      '#type'        => 'button',
      '#name'        => 'update-' . $row_number,
      '#value'       => $this->t('Update'),
      '#button_type' => 'primary',
      '#attributes'  => ['class' => ['vivus-update']],
    ];

    // Effect cancel button.
    $row['options_operations']['settings_cancel'] = [
      '#type'       => 'button',
      '#name'       => 'cancel-' . $row_number,
      '#value'      => $this->t('Cancel'),
      '#attributes' => ['class' => ['vivus-cancel']],
    ];

    // There are two functions attached to the remove button.
    // The submit function will be called first and used to remove selected row.
    // The callback function will then return the rendered rows.
    $row['operations'] = [
      '#row_number' => $row_number,
      // We need this - the call to getTriggeringElement when clicking the
      // remove button won't work without it.
      '#name'       => 'row-' . $row_number,
      '#type'       => 'submit',
      '#value'      => $this->t('Remove'),
      '#attributes' => [
        'class' => ['vivus-delete'],
        'title' => $this->t('Click here to delete this effect.'),
      ],
      '#submit'     => [[$this, 'ajaxFormSubmit']],
      '#ajax'       => [
        'callback' => [$this, 'ajaxFormCallback'],
        'progress' => [
          'type'    => 'throbber',
          'message' => NULL,
        ],
        'effect'   => 'fade',
      ],
      '#button_type' => 'danger',
    ];

    return $row;
  }

  /**
   * {@inheritdoc}
   */
  public function optionsSummary($animation) {
    $delay = empty($animation['delay']) ? 0 : $animation['delay'];
    $timing = vivus_timing_options();
    $reverse = $animation['reverse'] ? $this->t('True') : $this->t('False');
    return [
      $this->t('Duration: @title', ['@title' => $animation['duration']]),
      $this->t('Delay: @title', ['@title' => $delay]),
      $this->t('Reverse stack: @title', ['@title' => $reverse]),
      $this->t('Path timing: @title', ['@title' => $timing[$animation['path_timing']]]),
    ];
  }

}
