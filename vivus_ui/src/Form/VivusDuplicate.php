<?php

namespace Drupal\vivus_ui\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\vivus_ui\VivusManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form to duplicate vivus.
 *
 * @internal
 */
class VivusDuplicate extends FormBase {

  /**
   * The Vivus id.
   *
   * @var int
   */
  protected $vid;

  /**
   * The Animate selector manager.
   *
   * @var \Drupal\vivus_ui\VivusManagerInterface
   */
  protected $vivusManager;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs a new vivusDelete object.
   *
   * @param \Drupal\vivus_ui\VivusManagerInterface $vivus_manager
   *   The Animate selector manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(VivusManagerInterface $vivus_manager, TimeInterface $time) {
    $this->vivusManager = $vivus_manager;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('vivus.animation_manager'),
      $container->get('datetime.time'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vivus_duplicate_form';
  }

  /**
   * {@inheritdoc}
   *
   * @param array $form
   *   A nested array form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param string $vid
   *   The Vivus record ID to remove.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $vid = '') {
    $form['vivus_id'] = [
      '#type'  => 'value',
      '#value' => $vid,
    ];

    // New selector to duplicate effect.
    $form['selector'] = [
      '#title'         => $this->t('Selector'),
      '#type'          => 'textfield',
      '#required'      => TRUE,
      '#size'          => 64,
      '#maxlength'     => 255,
      '#default_value' => '',
      '#description'   => $this->t('Here, you can use HTML tag, class with dot(.) and ID with hash(#) prefix. Be sure your selector has plain text content. e.g. ".page-title" or ".block-title".'),
      '#placeholder'   => $this->t('Enter valid selector'),
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type'        => 'submit',
      '#value' => $this->t('Duplicate'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $vivus_id = $form_state->getValue('vivus_id');
    $is_new   = $vivus_id == 0;
    $selector = trim($form_state->getValue('selector'));

    if ($is_new) {
      if ($this->vivusManager->isVivus($selector)) {
        $form_state->setErrorByName('selector', $this->t('This selector is already exists.'));
      }
    }
    else {
      if ($this->vivusManager->findById($vivus_id)) {
        $vivus = $this->vivusManager->findById($vivus_id);

        if ($selector != $vivus['selector'] && $this->vivusManager->isVivus($selector)) {
          $form_state->setErrorByName('selector', $this->t('This selector is already added.'));
        }
      }
    }
  }

  /**
   * Form submission handler for the 'duplicate' action.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   A reference to a keyed array containing the current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $vivus_id = $values['vivus_id'];
    $selector = trim($values['selector']);
    $label    = ucfirst(trim(preg_replace("/[^a-zA-Z0-9]+/", " ", $selector)));
    $status   = 1;

    $vivus   = $this->vivusManager->findById($vivus_id);
    $comment = $vivus['comment'];
    $options = $vivus['options'];

    // The Unix timestamp when the vivus was most recently saved.
    $changed = $this->time->getCurrentTime();

    // Vivus save.
    $new_vid = $this->vivusManager->addVivus(0, $selector, $label, $comment, $changed, $status, $options);
    $this->messenger()
      ->addStatus($this->t('The selector %selector has been duplicated.', ['%selector' => $selector]));

    // Flush caches so the updated config can be checked.
    drupal_flush_all_caches();

    // Redirect to duplicated effect edit form.
    $form_state->setRedirect('vivus.edit', ['vid' => $new_vid]);
  }

}
