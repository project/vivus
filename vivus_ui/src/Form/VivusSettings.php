<?php

namespace Drupal\vivus_ui\Form;

use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a form that configures text animate settings.
 */
class VivusSettings extends ConfigFormBase {

  /**
   * The module extension list.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleExtensionList;

  /**
   * AdminController constructor.
   *
   * @param \Drupal\Core\Extension\ModuleExtensionList $extension_list_module
   *   The module extension list.
   */
  public function __construct(ModuleExtensionList $extension_list_module) {
    $this->moduleExtensionList = $extension_list_module;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('extension.list.module')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vivus_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Vivus flags for specific form labels and suffix.
    $experimental_label = ' <span class="vivus-experimental-flag">Experimental</span>';
    $beta_label = ' <span class="vivus-beta-flag">Beta</span>';
    $new_label = ' <span class="vivus-new-flag">New</span>';
    $ms_unit_label = ' <span class="vivus-unit-flag">ms</span>';
    $px_unit_label = ' <span class="vivus-unit-flag">px</span>';

    // Get current settings.
    $config = $this->config('vivus.settings');

    // Vivus module configuration.
    $form['settings'] = [
      '#type'  => 'details',
      '#title' => $this->t('Vivus config'),
      '#open'  => TRUE,
    ];

    // Let module handle load Vivus plugin.
    $form['settings']['load'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Load Vivus'),
      '#default_value' => $config->get('load'),
      '#description'   => $this->t("If enabled, this module will attempt to load the vivus plugin for your site."),
    ];

    // Show warning missing library and lock on cdn method.
    $method = $config->get('method');
    $method_lock_change = FALSE;
    if (!vivus_check_installed()) {
      $method = 'cdn';
      $method_lock_change = TRUE;
      $method_warning = $this->t('You cannot set local due to the Vivus.js library is missing. Please <a href=":downloadUrl" rel="external" target="_blank">Download the library</a> and and extract to "/libraries/vivus" directory.', [
        ':downloadUrl' => 'https://github.com/maxwellito/vivus/archive/master.zip',
      ]);

      // Hide library warning message.
      $form['settings']['hide'] = [
        '#type'          => 'checkbox',
        '#title'         => $this->t('Hide warning'),
        '#default_value' => $config->get('hide') ?? FALSE,
        '#description'   => $this->t("If you want to use the CDN without installing the local library, you can turn off the warning."),
      ];

      $form['settings']['method_warning'] = [
        '#type'   => 'item',
        '#markup' => '<div class="library-status-report">' . $method_warning . '</div>',
        '#states' => [
          'visible' => [
            ':input[name="load"]' => ['checked' => TRUE],
          ],
          'invisible' => [
            ':input[name="hide"]' => ['checked' => TRUE],
          ],
        ],
      ];
    }

    // Load method library from CDN or Locally.
    $form['settings']['method'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Vivus Method'),
      '#options'       => [
        'local' => $this->t('Local'),
        'cdn'   => $this->t('CDN'),
      ],
      '#default_value' => $method,
      '#description'   => $this->t('These settings control how the Vivus library is loaded. You can choose to load from the CDN (External source) or from the local (Internal library).'),
      '#disabled'      => $method_lock_change,
    ];

    // Production or minimized version.
    $form['settings']['minimized'] = [
      '#type'        => 'fieldset',
      '#title'       => $this->t('Development or Production version'),
      '#collapsible' => TRUE,
      '#collapsed'   => FALSE,
    ];
    $form['settings']['minimized']['minimized_options'] = [
      '#type'          => 'radios',
      '#title'         => $this->t('Choose minimized or non-minimized library.'),
      '#options'       => [
        0 => $this->t('Use non-minimized library (Development)'),
        1 => $this->t('Use minimized library (Production)'),
      ],
      '#default_value' => $config->get('minimized.options'),
      '#description'   => $this->t('These settings work both methods with locally and CDN library.'),
    ];

    // Load Vivus plugin Per-path.
    $form['settings']['url'] = [
      '#type'        => 'fieldset',
      '#title'       => $this->t('Load on specific URLs'),
      '#collapsible' => TRUE,
      '#collapsed'   => TRUE,
    ];
    $form['settings']['url']['url_visibility'] = [
      '#type'          => 'radios',
      '#title'         => $this->t('Load vivus on specific pages'),
      '#options'       => [
        0 => $this->t('All pages except those listed'),
        1 => $this->t('Only the listed pages'),
      ],
      '#default_value' => $config->get('url.visibility'),
    ];
    $form['settings']['url']['url_pages'] = [
      '#type'          => 'textarea',
      '#title'         => '<span class="element-invisible">' . $this->t('Pages') . '</span>',
      '#default_value' => _vivus_ui_array_to_string($config->get('url.pages')),
      '#description'   => $this->t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. An example path is %admin-wildcard for every user page. %front is the front page.", [
        '%admin-wildcard' => '/admin/*',
        '%front'          => '<front>',
      ]),
    ];

    // Vivus animation default options.
    $form['options'] = [
      '#type'  => 'details',
      '#title' => $this->t('Default options'),
      '#open'  => TRUE,
    ];

    // Defines what kind of animation will be used.
    $form['options']['animation_type'] = [
      '#title'         => $this->t('Animation type'),
      '#type'          => 'select',
      '#options'       => vivus_type_options(),
      '#default_value' => $config->get('options.type'),
      '#description'   => $this->t('Defines what kind of animation will be used: delayed, sync, oneByOne, scenario or scenario-sync.'),
      '#attributes'    => ['class' => ['vivus-type']],
    ];

    // Set whether to automatically start animating.
    $form['options']['start'] = [
      '#title'         => $this->t('Start'),
      '#type'          => 'select',
      '#options'       => vivus_start_options(),
      '#default_value' => $config->get('options.start'),
      '#description'   => $this->t('Defines how to trigger the animation (inViewport once the SVG is in the viewport, manual gives you the freedom to call draw method to start, autostart makes it start right now).'),
      '#attributes'    => ['class' => ['vivus-start']],
    ];

    // Animation duration, in frames.
    $form['options']['duration'] = [
      '#title'         => $this->t('Duration'),
      '#type'          => 'number',
      '#size'          => 9,
      '#maxlength'     => 9,
      '#default_value' => $config->get('options.duration'),
      '#description'   => $this->t('Animation duration, in frames.'),
      '#attributes'    => ['class' => ['vivus-duration']],
    ];

    // Time between the drawing of first and last path.
    $form['options']['delay'] = [
      '#title'         => $this->t('Delay'),
      '#type'          => 'number',
      '#size'          => 9,
      '#maxlength'     => 9,
      '#default_value' => $config->get('options.delay'),
      '#description'   => $this->t('Time between the drawing of first and last path, in frames (only for delayed animations).'),
      '#attributes'    => ['class' => ['vivus-delay']],
    ];

    // Set whether to automatically start animating.
    $form['options']['path_timing'] = [
      '#title'         => $this->t('Path timing'),
      '#type'          => 'select',
      '#options'       => vivus_timing_options(),
      '#default_value' => $config->get('options.pathTimingFunction'),
      '#description'   => $this->t('Timing animation function for each path element of the SVG.'),
      '#attributes'    => ['class' => ['vivus-path-timing']],
    ];

    // Whitespace extra margin between dashes.
    $form['options']['dash_gap'] = [
      '#title'         => $this->t('Dash gap'),
      '#type'          => 'number',
      '#size'          => 9,
      '#maxlength'     => 9,
      '#default_value' => $config->get('options.dashGap'),
      '#description'   => $this->t('Whitespace extra margin between dashes. Increase it in case of glitches at the initial state of the animation.'),
      '#attributes'    => ['class' => ['vivus-dash-gap']],
      '#field_suffix'  => $px_unit_label,
    ];

    // Force the browser to re-render all updated path items.
    $form['options']['force_render'] = [
      '#title'         => $this->t('Force render'),
      '#type'          => 'checkbox',
      '#default_value' => $config->get('options.forceRender'),
      '#description'   => $this->t("Force the browser to re-render all updated path items. By default, the value is true on IE only. (check the 'troubleshoot' section for more details)."),
      '#attributes'    => ['class' => ['vivus-force-render']],
    ];

    // Reverse the order of execution.
    $form['options']['reverse_stack'] = [
      '#title'         => $this->t('Reverse stack'),
      '#type'          => 'checkbox',
      '#default_value' => $config->get('options.reverseStack'),
      '#description'   => $this->t("Reverse the order of execution. The default behaviour is to render from the first 'path' in the SVG to the last one. This option allow you to reverse the order."),
      '#attributes'    => ['class' => ['vivus-reverse-stack']],
    ];

    // Removes all extra styling on the SVG, and leaves it as original.
    $form['options']['self_destroy'] = [
      '#title'         => $this->t('Self destroy'),
      '#type'          => 'checkbox',
      '#default_value' => $config->get('options.selfDestroy'),
      '#description'   => $this->t("Removes all extra styling on the SVG, and leaves it as original."),
      '#attributes'    => ['class' => ['vivus-self-destroy']],
    ];

    // Vivus preview.
    $form['additional_options'] = [
      '#type'   => 'details',
      '#title'  => $this->t('Additional options'),
      '#open'   => FALSE,
    ];

    // Enable animation looping.
    $form['additional_options']['loop'] = [
      '#title'         => $this->t('Loop') . $beta_label,
      '#type'          => 'checkbox',
      '#default_value' => $config->get('additional.loop'),
      '#description'   => $this->t("If enabled, element will repeat the animation."),
      '#attributes'    => ['class' => ['vivus-loop']],
    ];

    // Switch method between the animations.
    $form['additional_options']['switch'] = [
      '#title'         => $this->t('Switch animation') . $new_label . $experimental_label,
      '#type'          => 'select',
      '#options'       => vivus_switch_options(),
      '#default_value' => $config->get('additional.switch'),
      '#description'   => $this->t('Defines how animations change when a loop is enabled or has more than one animation (Default once the SVG will replace by another animation draw, Erase gives you rewind draw animation, Fade out makes disappear to start next animation).'),
      '#attributes'    => ['class' => ['vivus-switch']],
    ];

    // Whitespace extra margin between dashes.
    $form['additional_options']['initial_delay'] = [
      '#title'         => $this->t('Initial delay'),
      '#type'          => 'number',
      '#size'          => 6,
      '#maxlength'     => 9,
      '#default_value' => $config->get('additional.initialDelay'),
      '#description'   => $this->t('Sets the initial delay before the animation starts.'),
      '#attributes'    => ['class' => ['vivus-initial-delay']],
      '#field_suffix'  => $ms_unit_label,
    ];

    // Whitespace extra margin between dashes.
    $form['additional_options']['display_time'] = [
      '#title'         => $this->t('Display time'),
      '#type'          => 'number',
      '#size'          => 6,
      '#maxlength'     => 9,
      '#default_value' => $config->get('additional.displayTime'),
      '#description'   => $this->t('Sets the minimum display time for each animation before replacing.'),
      '#attributes'    => ['class' => ['vivus-display-time']],
      '#field_suffix'  => $ms_unit_label,
    ];

    // Vivus preview.
    $form['preview'] = [
      '#type'   => 'details',
      '#title'  => $this->t('Effect preview'),
      '#open'   => TRUE,
    ];

    $svg_example = '/' . $this->moduleExtensionList->getPath('vivus_ui') . "/img/polaroid.svg";

    // Vivus animation sample svg.
    $form['preview']['sample'] = [
      '#type'         => 'markup',
      '#markup'       => '<div class="vivus__preview bloc bloc-scenario"><object id="svgObject" type="image/svg+xml" data="' . $svg_example . '"></object></div>',
      '#allowed_tags' => [
        'object',
        'svg',
        'div',
      ],
    ];

    // Replay button for preview vivus current configs.
    $form['preview']['replay'] = [
      '#value'      => $this->t('Replay'),
      '#type'       => 'button',
      '#attributes' => ['class' => ['vivus__replay']],
    ];

    $form['preview']['rewind'] = [
      '#value'      => $this->t('Rewind'),
      '#type'       => 'button',
      '#attributes' => ['class' => ['vivus__rewind']],
    ];

    // The vivus settings library.
    $form['#attached']['library'][] = 'vivus_ui/vivus.settings';

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    // Save the updated vivus settings.
    $this->config('vivus.settings')
      ->set('load', $values['load'])
      ->set('hide', isset($values['hide']) && $values['hide'] !== 0 ?? FALSE)
      ->set('method', $values['method'])
      ->set('minimized.options', $values['minimized_options'])
      ->set('url.visibility', $values['url_visibility'])
      ->set('url.pages', _vivus_ui_string_to_array($values['url_pages']))
      ->set('options.type', $values['animation_type'])
      ->set('options.start', $values['start'])
      ->set('options.duration', $values['duration'])
      ->set('options.delay', $values['delay'])
      ->set('options.pathTimingFunction', $values['path_timing'])
      ->set('options.dashGap', $values['dash_gap'])
      ->set('options.forceRender', $values['force_render'])
      ->set('options.reverseStack', $values['reverse_stack'])
      ->set('options.selfDestroy', $values['self_destroy'])
      ->set('additional.loop', $values['loop'])
      ->set('additional.switch', $values['switch'])
      ->set('additional.initialDelay', $values['initial_delay'])
      ->set('additional.displayTime', $values['display_time'])
      ->save();

    // Flush caches so the updated config can be checked.
    drupal_flush_all_caches();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'vivus.settings',
    ];
  }

}
