<?php

namespace Drupal\vivus_ui\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\vivus_ui\VivusManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides a form to remove CSS selector.
 *
 * @internal
 */
class VivusDelete extends ConfirmFormBase {

  /**
   * The Animate selector.
   *
   * @var int
   */
  protected $vivus;

  /**
   * The Animate selector manager.
   *
   * @var \Drupal\vivus_ui\VivusManagerInterface
   */
  protected $vivusManager;

  /**
   * Constructs a new vivusDelete object.
   *
   * @param \Drupal\vivus_ui\VivusManagerInterface $vivus_manager
   *   The Animate selector manager.
   */
  public function __construct(VivusManagerInterface $vivus_manager) {
    $this->vivusManager = $vivus_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('vivus.animation_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vivus_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to remove %selector from vivus selectors?', ['%selector' => $this->vivus['selector']]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   *
   * @param array $form
   *   A nested array form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param int $vid
   *   The Vivus record ID to remove.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $vid = 0) {
    if (!$this->vivus = $this->vivusManager->findById($vid)) {
      throw new NotFoundHttpException();
    }
    $form['vivus_id'] = [
      '#type'  => 'value',
      '#value' => $vid,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $vivus_id = $form_state->getValue('vivus_id');
    $this->vivusManager->removeVivus($vivus_id);
    $this->logger('user')
      ->notice('Deleted %selector', ['%selector' => $this->vivus['selector']]);
    $this->messenger()
      ->addStatus($this->t('The vivus selector %selector was deleted.', ['%selector' => $this->vivus['selector']]));

    // Flush caches so the updated config can be checked.
    drupal_flush_all_caches();

    $form_state->setRedirectUrl($this->getCancelUrl());
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('vivus.admin');
  }

}
