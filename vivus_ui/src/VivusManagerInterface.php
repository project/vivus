<?php

namespace Drupal\vivus_ui;

/**
 * Provides an interface defining a vivus manager.
 */
interface VivusManagerInterface {

  /**
   * Returns if this vivus css selector is added.
   *
   * @param string $selector
   *   The vivus css selector to check.
   *
   * @return bool
   *   TRUE if the vivus css selector is added, FALSE otherwise.
   */
  public function isVivus($selector);

  /**
   * Finds an added vivus css selector by its ID.
   *
   * @return string|false
   *   Either the added vivus selector or FALSE if none exist with that ID.
   */
  public function loadVivus();

  /**
   * Add a vivus css selector.
   *
   * @param int $vivus_id
   *   The vivus id for edit.
   * @param string $selector
   *   The vivus selector to add.
   * @param string $label
   *   The label of vivus selector.
   * @param string $comment
   *   The comment for vivus options.
   * @param int $changed
   *   The expected modification time.
   * @param int $status
   *   The status for vivus.
   * @param string $options
   *   The vivus selector options.
   *
   * @return int|null|string
   *   The last insert ID of the query, if one exists.
   */
  public function addVivus($vivus_id, $selector, $label, $comment, $changed, $status, $options);

  /**
   * Remove a vivus css selector.
   *
   * @param int $vivus_id
   *   The vivus id to remove.
   */
  public function removeVivus($vivus_id);

  /**
   * Finds all added vivus css selector.
   *
   * @param array $header
   *   The vivus header to sort selector and label.
   * @param string $search
   *   The vivus search key to filter selector.
   * @param int|null $status
   *   The vivus status to filter selector.
   *
   * @return \Drupal\Core\Database\StatementInterface
   *   The result of the database query.
   */
  public function findAll(array $header, $search, $status);

  /**
   * Finds an added vivus css selector by its ID.
   *
   * @param int $vivus_id
   *   The ID for an added vivus selector.
   *
   * @return string|false
   *   Either the added vivus selector or FALSE if none exist with that ID.
   */
  public function findById($vivus_id);

}
