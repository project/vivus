INTRODUCTION
------------

vivus, bringing your SVGs to life

Vivus is a lightweight JavaScript class (with no dependencies) that allows you
to animate SVGs, giving them the appearance of being drawn.

There are a variety of different animations available, as well as the option
to create a custom script to draw your SVG in whatever way you like.


FEATURES
--------

'Vivus' library is:

  - Simply Ease-to-Use

  - Lightweight and Efficient

  - Diverse svg animation effects

  - Cross-browser and Responsive

  - Usage with Javascript

  - Customizable


REQUIREMENTS
------------

'Vivus.js' library:

  - https://github.com/maxwellito/vivus/archive/master.zip


INSTALLATION
------------

Download and install vivus module - Attach Vivus library
========================================================

1. Download 'Vivus' module:
   - https://www.drupal.org/project/vivus

2. Extract and place it in the root of contributed modules directory i.e.
   /modules/contrib/vivus or /modules/vivus

3. Create a libraries directory in the root, if not already there i.e.
   /libraries

4. Create a 'vivus' directory inside it i.e.
   /libraries/vivus

5. Download 'Vivus.js' library:
   https://github.com/maxwellito/vivus/archive/master.zip

6. Place it in the /libraries/vivus directory i.e. Required files:
  - /libraries/vivus/dist/vivus.js
  - /libraries/vivus/dist/vivus.min.js

7. Go to Extend menu "/admin/modules" in your Drupal administration.

8. Select and install 'Vivus' module to enable it.

9. It is done, now you can call vivus in your theme or module js file.


Install vivus UI module - User interface for using vivus
========================================================

1. For using Vivus without need to coding, install 'Vivus UI' too.

2. If you installed "Vivus UI" then Go to "Vivus" admin config page in
   your Drupal administration configuration menu to change default settings.
   - /admin/config/user-interface/vivus

3. Now go to "Svg animate" page in your Drupal admin structure menu to create
   and manage svg animations using Vivus.
   - /admin/structure/vivus

4. Click on "Add animation" in top of the vivus overview page.

5. Enter you existing svg file ID without '#' prefix.

6. Add animation typo and change their options and save.

7. Refresh your page, Enjoy that.


USAGE
-----

Vivus - Code usage
========================================================

As I said, no dependencies here. All you need to do is include the scripts.

Inline SVG

<svg id="my-svg">
  <path...>
  <path...>
  <path...>
</svg>

<script>
  new Vivus('my-svg', {duration: 200}, myCallback);
</script>

Dynamic load

<object id="my-svg" type="image/svg+xml" data="link/to/my.svg"></object>

<script>
  new Vivus('my-svg', { duration: 200 }, myCallback);
</script>

or

<div id="my-div"></div>

<script>
  new Vivus('my-div', { duration: 200, file: 'link/to/my.svg' }, myCallback);
</script>

By default, the object created will take the size of the parent element,
this one must have a height and width or your SVG might not appear.

If you need to edit this object, it is accessible in the onReady callback:

new Vivus('my-div-id', {
  file: 'link/to/my.svg',
  onReady: function (myVivus) {
    // `el` property is the SVG element
    myVivus.el.setAttribute('height', 'auto');
  }
});


Vivus UI usage - User interface
========================================================

1. Go to "Svg animate" admin overview page '/admin/structure/vivus'
   in your Drupal administration structure menu.

2. Click on "Add animation"

3. Enter valid ID of existing svg without '#' prefix.

3. Then you can add one or more animation by select animation type and
   click on "Add more" for more animations.

4. You can change some setting option if you need such as:

   - Type:
     Defines what kind of animation will be used: delayed, sync, oneByOne,
     scenario or scenario-sync. [Default: delayed]

   - Start:
     Defines how to trigger the animation (inViewport once the SVG is in
     the viewport, manual gives you the freedom to call draw method to start,
     autostart makes it start right now). [Default: inViewport]

   - Duration:
     Animation duration, in frames. [Default: 200]

   - Delay:
     Time between the drawing of first and last path, in frames (only for
     delayed animations).

   - Path timing:
     Timing animation function for each path element of the SVG.

   - Dash gap:
     Whitespace extra margin between dashes. Increase it in case of
     glitches at the initial state of the animation. [Default: 2]

   - Force render:
     Force the browser to re-render all updated path items. By default,
     the value is true on IE only.

   - Reverse stack:
     Reverse the order of execution. The default behaviour is to render from
     the first 'path' in the SVG to the last one. This option allow you to
     reverse the order. [Default: false]

   - Self destroy:
     Removes all extra styling on the SVG, and leaves it as original.

   - Loop:
     If enabled, element will repeat the animation.

   - Initial Delay:
     Delay before starting your animation.

   - Display time:
     The minimum display time for each animation before replacing.

5. Save animations and that's it!

6. Enjoy svg animation with Vivus!


How does it Work?
-----------------

1. Enable "Vivus" module, Follow INSTALLATION in above.

2. Pick svg ID you want to animate to it by using right-click on your
   website, choose "Inspect" or "View page source (CTRL + U)" of context menu
   in your Chrome browser and find exact valid existing svg ID.

   - Use ID WITHOUT hash(#) prefix.
     e.g. "mySvgID".

   * IMPORTANT:
      - If your svg doesn't have an ID, you need to edit your svg in some
        editor like "Notepad" or "Notepad++" and add a special ID.

3. Add animation in admin to animate your website svg, Follow USAGE in above.

4. Refresh your website where that your svg is there.

5. Enjoy that.

Animations can improve the UX of an interface, but keep in mind that they can
also get in the way of your users!


MAINTAINERS
-----------

Current module maintainer:

 * Mahyar Sabeti - https://www.drupal.org/u/mahyarsbt
